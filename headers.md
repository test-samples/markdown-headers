# Overview

This document describes how a booking is stored and how each element works to create one itinerary.

A booking is made up of several elements:

1. The booking itself
2. Contact information
3. Flights
4. Accommodation
5. Cruise
6. Passengers

Obviously this doesn't explain a booking fully, so below is an outline of what each element is used for and how they link together.

## The booking itself

## Contact information

## Flights

## Accommodation

## Cruise

## Passengers