# Bloot

When pushing to Dokku (10.10.10.221) two processes will run:

1. The `web` process, this just runs the default buildpack server to keep PHP running.
2. A `worker` process which just keeps running `php artisan queue:listen` so that jobs can be queued.

These can be found in `Procfile`. 